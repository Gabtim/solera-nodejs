'use strict'

//Requires
var express = require('express');
var bodyParser = require('body-parser');

//Express
var app = express();

//Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Routes
const api_routes = require('./src/routes/api.routes');
app.get('/', (req, res) => {
    return res.status(200).send({
        message: "Solera - NODEJS"
    })
})
app.use('/api', api_routes);

module.exports = app;