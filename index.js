'use strict'
require('dotenv').config()
require('./src/database');

var app = require('./app');
var port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`[App]: Listening on port ${port}`);
  })
  .on('error', e => logger.error(e));