const db = require('../database');
const bcrypt = require('bcryptjs');
const userCtrl = {};

userCtrl.login = async (req, res) => {
    try {
        const { user, password } = req.body;
        var userFind={}
        if (!(user && password)) {
            return res.status(400).json({error_message: "Debe ingresar el usuario y la contraseña"});
        }
        
        const [rows] = await db.query("SELECT * FROM `Users` WHERE `username`=?", [user])
        userFind = rows[0]
        if(userFind){
            bcrypt.compare(password, userFind.password, (err, check) =>{
                if(check){
                    return res.status(200).json({
                        status: "success",
                        fullname: userFind.fullname
                    })
                }else{
                    return res.status(400).json({error_message : "Las credenciales son incorrectas."});
                }
            })
        }else{
            return res.status(400).json({error_message : "El usuario no existe."});
        }
    
    } catch (err) {
        return res.status(400).json({error_message : err.toString()});
    }
}

module.exports = userCtrl;