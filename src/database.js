const mysql = require('mysql2/promise');

const { DB_USER, DB_PASS, DB_HOST, DB_NAME } = process.env;

const connection = mysql.createPool({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASS,
  database: DB_NAME,
});


module.exports = connection;